﻿namespace Bakery.Data
{
    public class User
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; } 
        public string Lastname { get; set; }
        public string Nickname { get; set; } 
        public int Age { get; set; }
        public int Tel { get; set; }
        public Address Address { get; set; }
        public string Email { get; set; }
    }
}
